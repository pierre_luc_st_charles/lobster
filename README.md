This directory contains a 'cleaned' version of the optimal LOBSTER configuration as presented in
the 2014 WACV paper 'Improving Background Subtraction Using Local Binary Similarity Patterns'.

The main class used for background subtraction is `BackgroundSubtractorLOBSTER`; all other files
contain either dependencies, utilities or interfaces for this method. It is based on OpenCV's
`BackgroundSubtractor` interface, and has been tested with versions 2.4.5 -> 2.4.9. By default,
its constructor uses the parameters suggested in the paper.

TL;DR :

```cpp
BackgroundSubtractorLOBSTER bgs(/*...*/);
bgs.initialize(/*...*/);
for(/*all frames in the video*/) {
    //...
    bgs(input,output);
    //...
}
```

**Note**: this repository is kept here as a stand-alone implementation reference; for the latest
version of the algorithm (with better optimization), see [our framework on github](https://github.com/plstcharles/litiv).

See LICENSE.txt for terms of use and contact information.
